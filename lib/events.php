<?
namespace Manao\Metadata;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset,
	Bitrix\Main\SystemException;
use Bitrix\Main\SiteTable;
Loc::loadMessages(__FILE__);

class Events
{
	function PanelCreate () {
			global $APPLICATION, $USER;

			$page = \Manao\Metadata\Metadata::getUriPage(); 
			
			$arIBlock = \Manao\Metadata\Metadata::getIBlockArray();
			$arIBlockElement = \Manao\Metadata\Metadata::getIBlockElement($page);
			
			$setUrlProperties = null;
			if (empty($arIBlockElement)) {
				$arIBlockProperties = \Manao\Metadata\Metadata::getIBlockProp();
				$setUrlProperties = "
				BX.addCustomEvent(seopopup, 'onWindowRegister', function(){  
					this.DIV.querySelector('#tr_PROPERTY_".$arIBlockProperties["META_URL"]["ID"]." input').value = '".$page."';
				});";
			}
			
			$scriptIn = "<script type'text/javascript'>
			function seopop () {
				window.seopopup = (new BX.CAdminDialog({'content_url':'/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=".$arIBlock['ID']."&type=manao_metadata&ID=".$arIBlockElement[0]['IBLOCK_ELEMENT_ID']."&lang=ru&filter_section=0&bxpublic=Y&from_module=iblock'}));
				seopopup.Show(); 
				".$setUrlProperties."
			}
			</script>";
			Asset::getInstance()->addString($scriptIn);
			
			if ($USER->isAdmin()) {
				$APPLICATION->AddPanelButton(
					Array(
						"ID" => "manao.metadata",
						"TEXT" => Loc::getMessage("METADATA_BUTTON_TITLE"),
						"TYPE" => "BIG", 
						"MAIN_SORT" => 1300,
						"HREF" => "javascript:seopop()", 
						"ICON" => "bx-panel-button-icon bx-panel-edit-page-icon", 
						"HINT" => array(
							"TITLE" => Loc::getMessage("METADATA_BUTTON_HEADER"),
							"TEXT" => Loc::getMessage("METADATA_BUTTON_DESCRIPTION")
						),
					)
				);
			}
	}
	
	function MetadataSet () {
			global $APPLICATION;
			
			if (\Bitrix\Main\Loader::includeModule('iblock')) {
				$page = \Manao\Metadata\Metadata::getUriPage(); 
				$arIBlock = \Manao\Metadata\Metadata::getIBlockArray();
				$arIBlockElement = \Manao\Metadata\Metadata::getIBlockElement($page);
				
					$arFilter = Array(
						"IBLOCK_ELEMENT_ID" => $arIBlockElement[0]["IBLOCK_ELEMENT_ID"],
						"=ELEMENT.IBLOCK_ID" => $arIBlock['ID']
					);
						
					$IBlockElementProperties = \Manao\Metadata\IblockElementPropertyTable::getProperties(
						array(
							"select" => array('*'),
							"filter" => $arFilter,
						),
						function($prop){
							$propArr = array();
							while($data = $prop->fetch()) {
								if (empty($data['PROPERTY_CODE'])) continue;
								$propArr[$data['PROPERTY_CODE']] = $data;
							}
							return $propArr;
						}
					);
				
					
					if (!empty($IBlockElementProperties)) {
						
						if (!empty($IBlockElementProperties['META_HEADER']['VALUE'])) {
							$APPLICATION->SetTitle($IBlockElementProperties['META_HEADER']['VALUE']);
						}
						if (!empty($IBlockElementProperties['META_TITLE']['VALUE'])) {
							$APPLICATION->SetPageProperty("title", $IBlockElementProperties['META_TITLE']['VALUE']);
						}
						if (!empty($IBlockElementProperties['META_DESCRIPTION']['VALUE'])) {
							$value = unserialize($IBlockElementProperties['META_DESCRIPTION']['VALUE']);
							$APPLICATION->SetPageProperty("description", $value["TEXT"]);
						}
						if (!empty($IBlockElementProperties['META_OTHERMETA']['VALUE'])) {
							$value = unserialize($IBlockElementProperties['META_OTHERMETA']['VALUE']);
							Asset::getInstance()->addString($value["TEXT"]);
						}
					}
			}
	}
	
	function OnBeforeIBlockUpdateHandler (&$arFields) {
			try {
				if ( $arFields["CODE"] != "manao_metadata" ) {	
						throw new SystemException(Loc::getMessage("METADATA_NO_CHANGE_CODE"));
				}
			}
			catch (SystemException $exception) {
				echo $exception->getMessage();
				return false;
			} 
	}
}
?>