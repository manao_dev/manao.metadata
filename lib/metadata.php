<?php
namespace Manao\Metadata;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Iblock,
	Bitrix\Main\Web\Uri,
	Bitrix\Main\Application;
Loc::loadMessages(__FILE__);

class Metadata
{
	const IBLOCK_TYPE = "manao_metadata";
	
	function getIBlockArray () {
		if (\Bitrix\Main\Loader::includeModule('iblock')) {
			
			$arResult = array();
			
			$arFilter = Array(
				"CODE"=> self::IBLOCK_TYPE, 
				"ACTIVE"=>"Y"
			);
			
			$IBlockList = \Bitrix\Iblock\IblockTable::GetList(
				array(
					"filter" => $arFilter,
				)
			);	
			
			while($data = $IBlockList->Fetch())
			{
				$arResult = $data;
			}
		
			return $arResult;
		}
	}
	
	function getIBlockProp () {
		$arIBlock = self::getIBlockArray();
		$arResult = array();
		if (\Bitrix\Main\Loader::includeModule('iblock')) {
			$arResult = array();
			
			$arFilter = Array(
				"ACTIVE"=>"Y",
				"IBLOCK_ID" => $arIBlock['ID']
			);
			
			$IBlockList = \Bitrix\Iblock\PropertyTable::GetList(
				array(
					"filter" => $arFilter,
				)
			);	
			
			while($data = $IBlockList->Fetch())
			{
				if(empty($data['CODE'])) continue;
				$arResult[$data['CODE']] = $data;
			}
		}
		
		return $arResult;
	}
	
	function getIBlockElement ($page) {
		$arIBlock = self::getIBlockArray();
		
		$arResult = array();
		if (\Bitrix\Main\Loader::includeModule('iblock')) {
			
			$arFilter = Array(
				"VALUE" => $page,
				"=PROPERTY.CODE" => "META_URL",
				"=ELEMENT.IBLOCK_ID" => $arIBlock['ID']
			);
			
			$arResult = $IBlockElementURL = \Manao\Metadata\IblockElementPropertyTable::getProperties(
				array(
					"select" => array('*'), 
					"filter" => $arFilter,
				)
			);
			
			return $arResult;
		}
	}
	
	
	function getUriPage () {
		$request = Application::getInstance()->getContext()->getRequest(); 
		return str_replace("?clear_cache=Y", "", $request->getRequestUri());
	}

}

