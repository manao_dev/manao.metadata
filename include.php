<?
Bitrix\Main\Loader::registerAutoLoadClasses(
	"manao.metadata",
	array(
		"Manao\Metadata\IblockElementPropertyTable" => "lib/elementpropertytable.php",
		"Manao\Metadata\Metadata" => "lib/metadata.php",
		"Manao\Metadata\Events" => "lib/events.php",
	)
);

?>

