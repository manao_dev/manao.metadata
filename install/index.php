<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class manao_metadata extends CModule
{
	var $MODULE_ID = "manao.metadata";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $PARTNER_NAME;
	var $PARTNER_URI;
	var $IBLOCK_TYPE = "manao_metadata";
	var $ERROR = array();

	function manao_metadata() {
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		
		if (is_array($arModuleVersion) && isset($arModuleVersion["VERSION"]))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = METADATA_VERSION;
			$this->MODULE_VERSION_DATE = METADATA_VERSION_DATE;
		}

		$this->MODULE_NAME = Loc::getMessage("METADATA_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("METADATA_MODULE_DESCRIPTION");
		
		$this->PARTNER_NAME = "Manao"; 
		$this->PARTNER_URI = "https://manao.by";
	}
	
	function DoInstall()
	{
		global $APPLICATION, $step;
		
		$step = (int) $step;
		$this->ERROR = false;

		if(!ModuleManager::isModuleInstalled("iblock"))
			$errors = Loc::getMessage("METADATA_UNINS_IBLOCK");
		else
		{
				if( $step < 2 )
				{
					$APPLICATION->IncludeAdminFile(
						Loc::getMessage("METADATA_INSTALL_TITLE"),
						$_SERVER["DOCUMENT_ROOT"].getLocalPath("modules/".$this->MODULE_ID."/install/step1.php")
					);
					
				}
				elseif ( $step == 2 )
				{
						$this->InstallFormOptions($_REQUEST["SELECT_SITE"]);
						
						$EventManager = \Bitrix\Main\EventManager::getInstance();
						$EventManager->registerEventHandler('main', 'OnPanelCreate', $this->MODULE_ID, 'Manao\Metadata\Events', 'PanelCreate');
						$EventManager->registerEventHandler('main', 'OnEpilog', $this->MODULE_ID, 'Manao\Metadata\Events', 'MetadataSet');
						$EventManager->registerEventHandler('iblock', 'OnBeforeIBlockUpdate', $this->MODULE_ID, 'Manao\Metadata\Events', 'OnBeforeIBlockUpdateHandler');

						RegisterModule($this->MODULE_ID);	
						$APPLICATION->IncludeAdminFile(
							Loc::getMessage("METADATA_INSTALL_TITLE"),
							$_SERVER["DOCUMENT_ROOT"].getLocalPath("modules/".$this->MODULE_ID."/install/step2.php")
						);
				}
		}
	}
	
	function DoUninstall()
	{
		global $APPLICATION, $step;

		$step = (int) $step;
		
			if( $step < 2 )
			{
				$APPLICATION->IncludeAdminFile(
					Loc::getMessage("METADATA_UNINSTALL_TITLE"), 
					$_SERVER["DOCUMENT_ROOT"].getLocalPath("modules/".$this->MODULE_ID."/install/unstep1.php")
				);
			}
			elseif( $step == 2 )
			{
				$this->ERROR = false;
				$this->DelIblocks();
				
				$EventManager = \Bitrix\Main\EventManager::getInstance();
				$EventManager->unRegisterEventHandler('main', 'OnPanelCreate', $this->MODULE_ID);
				$EventManager->unRegisterEventHandler('main', 'OnEpilog', $this->MODULE_ID);
				$EventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockUpdate', $this->MODULE_ID);
				
				UnRegisterModule($this->MODULE_ID);
				
				$APPLICATION->IncludeAdminFile(
					Loc::getMessage("METADATA_UNINSTALL_TITLE"), 
					$_SERVER["DOCUMENT_ROOT"].getLocalPath("modules/".$this->MODULE_ID."/install/unstep2.php")
				);
			}
		
	}	
	
	//функция создания типа инфоблока
	function AddIblockType($arFieldsIBT){
		$IBlockType = $this->IBLOCK_TYPE;
		
			$arFilter = array (
				"ID" => $iblockType
			);
			$IBlockTypeCreate = \Bitrix\Iblock\TypeTable::getList(
				array (
					"filter" => $arFilter
				)
			)->fetch();

			if (!$IBlockTypeCreate) {				
				$IBlockTypenL = $arFieldsIBT;
				unset($IBlockTypenL['LANG']);
				
				$resIBT = \Bitrix\Iblock\TypeTable::add($IBlockTypenL);
				
				if (CACHED_b_iblock_type !== false) {
					$cacheBlock = new \Bitrix\Main\Data\ManagedCache;
					$cache = $cacheBlock->cleanDir(\Bitrix\Iblock\TypeTable::getTableName());
				}	
			}
			else {
				$this->ERROR[] = Loc::getMessage("METADATA_IBLOCK_TYPE_NOT_INSTALLED");	
			}			

		return $IBlockType;
	}
	
	// функция добавления инфоблока
	function AddIblock($arFieldsIB){
			$arFilter = array (
				"IBLOCK_TYPE_ID" => $this->IBLOCK_TYPE,
				"CODE" => $this->IBLOCK_TYPE
			);
			$IBlockCreate = \Bitrix\Iblock\IblockTable::getList(
				array (
					"filter" => $arFilter
				)
			)->fetch();
			
			if (!$IBlockCreate) {
				$IBlock = new CIBlock;
				$iblockID = $IBlock->Add($arFieldsIB);
				//$obBlock = new \Bitrix\Iblock\IblockTable;
				//$iblockID = $obBlock::add($arFieldsIB);
			}
			else {
				$this->ERROR[] = Loc::getMessage("METADATA_IBLOCK_NOT_INSTALLED");	
			}

		return $iblockID;
	}
	
	//добавление свойств инфоблока
	function AddProp($arFieldsProp){

			$IBlockProperty = new CIBlockProperty;
			$propID = $IBlockProperty->Add($arFieldsProp);

		return $propID;
	}
	
	//удаление типа инфоблока при деинсталляции
	function DelIblocks(){
		if (\Bitrix\Main\Loader::includeModule('iblock')) {
			$deleted = \Bitrix\Iblock\TypeTable::delete($this->IBLOCK_TYPE);
			//$deleted = CIBlockType::Delete($this->IBLOCK_TYPE);
			$deletedLang = \Bitrix\Iblock\TypeLanguageTable::deleteByIblockTypeId($this->IBLOCK_TYPE);
		}		
	}
	
	private function IBlockTypeReturn () {
		return Array(
				'ID' => $this->IBLOCK_TYPE,
				'SECTIONS' => 'Y',
				'IN_RSS' => 'N',
				'SORT' => 500,
				'LANG' => \Bitrix\Iblock\TypeLanguageTable::add(
						Array(
							"IBLOCK_TYPE_ID" => $this->IBLOCK_TYPE,
							"LANGUAGE_ID" => "ru",
							"NAME" => Loc::getMessage("METADATA_MODULE_NAME"),
							"SECTIONS_NAME" => Loc::getMessage("METADATA_MODULE_NAME"),
						)
				)
			);
	}
	
	private function IBlockReturn ($site) {
		return Array(
					"ACTIVE" => "Y",
					"NAME" => Loc::getMessage("METADATA_MODULE_NAME"),
					"CODE" => $this->IBLOCK_TYPE,
					"IBLOCK_TYPE_ID" => $this->IBLOCK_TYPE,
					"SITE_ID" => $site,
					"GROUP_ID" => Array("1" => "W"),
					"RIGHTS_MODE" => "R",
					"FIELDS" => Array(
						"CODE" => Array(
							"DEFAULT_VALUE" => Array(
								"TRANS_CASE" => "L",
								"UNIQUE" => "Y",
								"TRANSLITERATION" => "Y",
								"TRANS_SPACE" => "-",
								"TRANS_OTHER" => "-"
							)
						)
					)
				);
	}
	
	private function IBlockPropertiesReturn () {
		return array(
					"META_URL" => 		
						array (
							"NAME" => Loc::getMessage("METADATA_IBLOCK_PROP_URL"),
							"IS_REQUIRED" => "Y",
							"PROPERTY_TYPE" => "S",
						),
					"META_TITLE" => 		
						array (
							"NAME" => Loc::getMessage("METADATA_IBLOCK_PROP_TITLE"),
							"PROPERTY_TYPE" => "S",
						),	
					"META_HEADER" => 		
						array (
							"NAME" => Loc::getMessage("METADATA_IBLOCK_PROP_HEADER"),
							"PROPERTY_TYPE" => "S",
						),
					"META_DESCRIPTION" => 		
						array (
							"NAME" => Loc::getMessage("METADATA_IBLOCK_PROP_DESCRIPTION"),
							"PROPERTY_TYPE" => "S",
							"USER_TYPE" => "HTML",
						),			
					"META_OTHERMETA" => 		
						array (
							"NAME" => Loc::getMessage("METADATA_IBLOCK_PROP_OTHERMETA"),
							"PROPERTY_TYPE" => "S",
							"USER_TYPE" => "HTML",
						),									
					);
	}
	
	private function CreateIblocks($site) {
		global $APPLICATION;
		if (!empty($this->ERROR))
		{
			$APPLICATION->ThrowException(implode('. ', $this->ERROR));
			return false;
		}
		
		if (\Bitrix\Main\Loader::includeModule('iblock')) {
			$arFieldsForType = $this->IBlockTypeReturn();

			if ($this->AddIblockType($arFieldsForType)){
				$arFieldsForIblock = $this->IBlockReturn($site);

				if ($iblockID = $this->AddIblock($arFieldsForIblock)){
					
					$arPropertiesList = $this->IBlockPropertiesReturn();

					$sort = 100;
					$arPropertiesID = array();
					foreach ($arPropertiesList as $key => $value) {
						$value["IBLOCK_ID"] = $iblockID;
						$value["CODE"] = $key;
						$value["ACTIVE"] = "Y";
						$value["SORT"] = $sort;
						$arPropertiesID[$value["CODE"]]["IBLOCK_ID"] = $value["IBLOCK_ID"];
						$arPropertiesID[$value["CODE"]]["ID"] = $this->AddProp($value);		
						$sort += 100;
					}
					
				}
			}
			
			return $arPropertiesID;
		
		}
	}
	
	function InstallFormOptions ($site) {
		global $APPLICATION;
		
			$arIBlock = $this->CreateIblocks($site);
						
			$arFormSettings = array(
				array(
					array("edit1", Loc::getMessage("METADATA_MODULE_NAME")),
					array("ACTIVE", Loc::getMessage("METADATA_OPTION_ACTIVE")),
					array("NAME", Loc::getMessage("METADATA_OPTION_NAME")), 
				 )
			);
						
			$iBlockID = null;
			foreach ($arIBlock as $key => $value) {
				$iBlockID = $value["IBLOCK_ID"];
				$arFormSettings[0][] = array(
					"PROPERTY_".$value["ID"], 
					Loc::getMessage("METADATA_IBLOCK_PROP_".str_replace('META_', '', $key))
				);
			}						
						
			// Сериализация
			$arFormFields = array();
			foreach ($arFormSettings as $key => $arFormFields)
			{
				$arFormItems = array();
				foreach ($arFormFields as $strFormItem)
				$arFormItems[] = implode('--#--', $strFormItem);

				$arStrFields[] = implode('--,--', $arFormItems);
			}
			
			$arSettings = array("tabs" => implode('--;--', $arStrFields));
						
			// Применяем настройки для всех пользователей для данного инфоблока
			$rez = CUserOptions::SetOption("form", "form_element_".$iBlockID, $arSettings, true, false);
	}
	
	
}
