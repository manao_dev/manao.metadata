<?
$MESS["METADATA_MODULE_NAME"] = "Метаданные";
$MESS["METADATA_MODULE_DESCRIPTION"] = "Модуль, позволяющий изменять метаданные на каждой из страниц сайта.";
$MESS["METADATA_INSTALL_TITLE"] = "Установка модуля метаданных";
$MESS["METADATA_UNINSTALL_TITLE"] = "Удаление модуля метаданных";
$MESS["METADATA_SELECT_INITITAL"] = "Выберите каталог для файлов модуля метаданных:";
$MESS["METADATA_DENIED"] = "доступ закрыт";
$MESS["METADATA_ADMIN"] = "администратор";
$MESS["METADATA_ERROR_EDITABLE"] = "Модуль не доступен в вашей редакции.";
$MESS["METADATA_IBLOCK_TYPE_NOT_INSTALLED"] = "Не установлен тип инфоблока: на сайте уже имеется данный тип инфоблока.";
$MESS["METADATA_IBLOCK_NOT_INSTALLED"] = "Данный инфоблок уже присутствует на сайте.";
$MESS["METADATA_IBLOCK_TYPE_DELETE_ERROR"] = "Ошибка удаления типа инфоблока.";

$MESS["METADATA_UNINS_IBLOCK"] = "Для работы модуля Каталог необходим модуль Инфоблоков.<br />Установите сначала модуль \"Инфоблоки\".";

$MESS["METADATA_IBLOCK_PROP_URL"] = "URL";
$MESS["METADATA_IBLOCK_PROP_TITLE"] = "Заголовок страницы (<title>)";
$MESS["METADATA_IBLOCK_PROP_DESCRIPTION"] = "Описание (description)";
$MESS["METADATA_IBLOCK_PROP_HEADER"] = "Заголовок контента (<h1>)";
$MESS["METADATA_IBLOCK_PROP_OTHERMETA"] = "Любая другая метаинформация";

$MESS["METADATA_OPTION_ACTIVE"] = "Активность";
$MESS["METADATA_OPTION_NAME"] = "*Название";
?>